const express = require("express");
const connectToDatabase = require("./db");
const Processos = require("./models/Processos");
const cors = require('cors');
const conf = require("./config");
const bodyParser = require("body-parser");
const xvideos = require('@rodrigogs/xvideos');
const app = express();


const port = 9003;
var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
}

app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json({
  limit: '50mb', extended: true
}));
app.use(bodyParser.urlencoded({
  limit: '50mb', extended: true
}));
app.use(cors(corsOptions));

app.get("/", async (req, res) => {
  try {
    const dashboardList = await xvideos.videos.dashboard({ page: 1 });
    res.send({ error: false, data: dashboardList});
  } catch (error) {
    res.send({error: true, data: error});
  }
});

app.post("/listar-home", async (req, res) => {
  const {link} = req.body;
  try {
    const detail = await xvideos.videos.details(link);
    res.send({ error: true, data: detail });
  } catch (error) {
    res.send({ error: true, data: error });
  }
});

app.listen(port, () => {
  console.log(`Server is booming on port 9003 Visit http://localhost:9003`);
});
