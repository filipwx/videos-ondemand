const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
const conf = require("./config");
let isConnected;

module.exports = connectToDatabase = () => {
  if (isConnected) {
    console.log("=> using existing database connection");
    return Promise.resolve();
  }

  console.log("=> using new database connection");
  let banco = process.env.MONGODB_URI || conf.connectioString;
  return mongoose.connect(banco).then(db => {
    isConnected = db.connections[0].readyState;
  });
};
