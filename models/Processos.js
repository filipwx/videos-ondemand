const mongoose = require("mongoose");
const ProcessosSchema = new mongoose.Schema({
  processo: {
    type: String,
    required: true,
    index: true,
    unique: true
  },
  link: {
    type: String,
    required: false,
  },
  tooltip: {
    type: String,
    required: false,
  },
  last_update: {
    type: Date,
  },
  ultima_atualizacao: {
    type: String,
  },
  createdAt: {
    type: Date,
    required: true,
    default: new Date()
  },
  lista_processos: [{
    data: {
      type: Date,
      required: false,
    },
    unidade: {
      type: String,
      required: false,
    },
    unidade_detalhe: {
      type: String,
      required: false,
    },
    usuario: {
      type: String,
      required: false,
    },
    usuario_detalhe: {
      type: String,
      required: false,
    },
    decricao: {
      type: String,
      required: false,
    },
  }]
});
module.exports = mongoose.model("Processos", ProcessosSchema);